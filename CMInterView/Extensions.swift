//
//  Extensions.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/6.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

extension UIView {
    func addSubViews(_ views: [UIView]) {
        views.forEach { [weak self] (v) in
            self?.addSubview(v)
        }
    }
}

extension UIStackView {
    func addArrangedSubviews(_ views: [UIView]) {
        views.forEach { [weak self] (v) in
            self?.addArrangedSubview(v)
        }
    }
}
