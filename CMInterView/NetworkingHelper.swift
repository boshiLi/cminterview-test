//
//  NetworkingHelper.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/5.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import Foundation

public enum NetworkingError: Error {
    
    case responseAndDataNil
    case jsonParseFail(error: Error, jsonString: String?)
    case error(Error)
    case noNetworkingConnect
    case othersError(code: Int)
}

public typealias ComplectionBlock<T: Decodable> = (Result<T, NetworkingError>) -> Swift.Void


class NetworkingHelper {
    
    static func createDataRequest<T: Decodable>(fromURL url: URL, model: T.Type, complection: @escaping ComplectionBlock<T>) -> URLSessionDataTask {
        
        var request: URLRequest = .init(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    complection(.failure(.error(error!)))
                    return
                }
                guard let response = response, let data = data else {
                    complection(.failure(NetworkingError.responseAndDataNil))
                    return
                }
                guard ((response as! HTTPURLResponse).statusCode == 200) else {
                    complection(
                        .failure(
                            .othersError(code: (response as! HTTPURLResponse).statusCode)
                        )
                    )
                    return
                }
                //success part
                do {
                    let dataModel = try JSONDecoder().decode(T.self, from: data)
                    complection(.success(dataModel))
                } catch {
                    if let jsonString = String(data: data, encoding: .utf8) {
                        complection(.failure(.jsonParseFail(error: error, jsonString: jsonString)))
                    } else {
                        complection(.failure(.jsonParseFail(error: error, jsonString: nil)))
                    }
                }
                
            }
        }
        return task
        
    }
    
    
}
