//
//  SecondViewModel.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/5.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import Foundation

enum NetWorkState {
    case initial
    case loading
    case loaded
    case error(message: String)
}

protocol SecondViewModelBindable: class {
    func didChangeState(_ state: NetWorkState)
    func didGetAlbums()
}

class SecondViewModel {
    
    weak var binder: SecondViewModelBindable!
    var cellVMs = [AlbumCellViewModel]()
    var albums: [Album] = .init()
    
    init(binder: SecondViewModelBindable) {
        self.binder = binder
    }
    
    func fetchAlbums() {
        self.binder.didChangeState(.loading)
        NetworkingHelper.createDataRequest(fromURL: URL(string: "https://jsonplaceholder.typicode.com/photos")!, model: [Album].self) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.binder.didChangeState(.loaded)
                self.albums = model
                self.cellVMs = self.createCellVMs(albums: self.albums)
                self.binder.didGetAlbums()
            case .failure(let error):
                switch error {
                case .responseAndDataNil:
                    self.binder.didChangeState(.error(message: "無資料"))
                case .jsonParseFail(let error, let jsonString):
                    print(error, jsonString as Any)
                case .error(let error):
                    print(error.localizedDescription)
                case .noNetworkingConnect:
                    self.binder.didChangeState(.error(message: "無網路連線"))
                case .othersError(let code):
                    self.binder.didChangeState(.error(message: "\(code)"))
                }
            }
        }.resume()
    }
    
        
    func createCellVMs(albums: [Album]) -> [AlbumCellViewModel] {
        return albums.compactMap({
            AlbumCellViewModel(id: "\($0.id)", title: $0.title, imageURL: $0.thumbnailUrl)
        })
    }
    
}
