//
//  SecondViewController.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/5.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

class SecondViewController: UICollectionViewController {
    
    private let cellPadding: CGFloat = 0.5
    private let numberOfColumns: Int = 4
    private var dataSource: AlbumCollecionViewDataSource?
    
    private var cellSize: CGSize {
        let width: CGFloat = (UIScreen.main.bounds.width / CGFloat(numberOfColumns)) - cellPadding * CGFloat(numberOfColumns)
        return CGSize(width: width, height: width)
    }
    
    
    
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var stateLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: 18)
        l.textColor = .white
        l.isHidden = true
        return l
    }()
    
    lazy var stateLabelConstraints: [NSLayoutConstraint] = {
        var layouts = [NSLayoutConstraint]()
        layouts.append(self.stateLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor))
        layouts.append(self.stateLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor))
        return layouts
    }()

    lazy var viewModel: SecondViewModel = .init(binder: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setupViews()
        self.viewModel.fetchAlbums()
    }
    
    func setupViews() {
        self.view.addSubViews([self.stateLabel])
        NSLayoutConstraint.activate(self.stateLabelConstraints)
    }

}

extension SecondViewController: PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return self.cellSize.height
    }
}

extension SecondViewController: SecondViewModelBindable {
    
    
    func didGetAlbums() {
        self.dataSource = AlbumCollecionViewDataSource(albums: self.viewModel.albums)
        self.collectionView.dataSource = self.dataSource
        self.collectionView.reloadData()
    }
    
    func didChangeState(_ state: NetWorkState) {
        switch state {
        case .initial:
            self.stateLabel.text = ""
            break
        case .loading:
            self.stateLabel.text = "Loading"
            self.stateLabel.isHidden = false
        case .loaded:
            self.stateLabel.isHidden = true
        case .error(let message):
            self.stateLabel.text  = message
            self.stateLabel.isHidden = false
        }
    }
    
}
