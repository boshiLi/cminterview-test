//
//  AlbumCollecionViewDataSource.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/6.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

class AlbumCollecionViewDataSource: NSObject {
    
    let albums: [Album]
    var cellVMs = [AlbumCellViewModel]()
        
    init(albums: [Album]) {
        self.albums = albums
        super.init()
        self.cellVMs = self.createCellVMs()
    }
    
    func createCellVMs() -> [AlbumCellViewModel] {
        return self.albums.compactMap({
            AlbumCellViewModel(id: "\($0.id)", title: $0.title, imageURL: $0.thumbnailUrl)
        })
    }
}

extension AlbumCollecionViewDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cellVMs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard self.cellVMs.indices.contains(indexPath.row) else { return .init() }
        let vm = self.cellVMs[indexPath.row]
        let cell = vm.cellInstance(cell: AlbumCell.self, collectionView: collectionView, atIndexPath: indexPath)
        cell.setup(viewModel: vm)
        return cell
    }
    
    
}
