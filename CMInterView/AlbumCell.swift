//
//  AlbumCellCollectionViewCell.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/6.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

class AlbumCell: UICollectionViewCell, CollecionCellConfigurable {
    
    lazy var idLabel: UILabel = {
        let l = UILabel()
        l.setContentHuggingPriority(.defaultHigh, for: .vertical)
        l.font = .systemFont(ofSize: 14, weight: .bold)
        l.textColor = .black
        l.numberOfLines = 1
        return l
    }()
    
    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.font = .systemFont(ofSize: 12, weight: .bold)
        l.textColor = .gray
        l.numberOfLines = 1
        l.lineBreakMode = .byTruncatingTail
        return l
    }()
    
    lazy var urlLabel: UILabel = {
        let l = UILabel()
        l.font = .systemFont(ofSize: 12, weight: .bold)
        l.textColor = .gray
        l.numberOfLines = 2
        l.textAlignment = .center
        return l
    }()
    
    
    
    lazy var stackView: UIStackView = {
        let s = UIStackView()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.spacing = 2
        s.axis = .vertical
        s.distribution = .fill
        s.alignment = .center
        return s
    }()
    
    lazy var imageView: WebImageView = {
        let i = WebImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    
    func setupView() {

        self.contentView.backgroundColor = .white
        
        
        stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 8).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8).isActive = true
        
        imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        stackView.addArrangedSubviews([self.idLabel, self.titleLabel, self.urlLabel])
        self.addSubViews([self.imageView, self.stackView])
        
    }

    
    override func layoutSubviews() {
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(viewModel: CollectionCellViewModelProtocol) {
        guard let vm = viewModel as? AlbumCellViewModel else { return }
        self.imageView.load(url: vm.imageURL)
        self.idLabel.text = vm.id
        self.titleLabel.text = vm.title
        self.urlLabel.text = vm.imageURL.absoluteString
    }
    
}
