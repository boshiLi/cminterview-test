//
//  ViewController.swift
//  CMInterViewTest
//
//  Created by Boshi Li on 2019/11/5.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

class FirtViewController: UIViewController {
    
    lazy var centerButton: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Request API", for: .normal)
        b.setTitleColor(.black, for: .normal)
        b.addTarget(self, action: #selector(self.pushToSecondView), for: .touchUpInside)
        return b
    }()
    
    lazy var centerButtonConstraint: [NSLayoutConstraint] = {
        var layouts = [NSLayoutConstraint]()
        layouts.append(self.centerButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor))
        layouts.append(self.centerButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor))
        return layouts
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.setupViews()
    }
    
    func setupViews() {
        self.view.addSubview(self.centerButton)
        NSLayoutConstraint.activate(centerButtonConstraint)
    }
    
    @objc func pushToSecondView() {
        
        let layout = PinterestLayout()
        layout.setCellPadding(1)
        layout.setNumberOfColumns(4)
        let secondView = SecondViewController.init(collectionViewLayout: layout)
        layout.delegate = secondView
        self.navigationController?.pushViewController(
            secondView,
            animated: true
        )
    }


}

