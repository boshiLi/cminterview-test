//
//  AlbumCellViewModel.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/6.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import UIKit

class AlbumCellViewModel: CollectionCellViewModelProtocol {
    
    let id: String
    let title: String
    let imageURL: URL
    
    init(id: String, title: String, imageURL: String) {
        self.id = id
        self.title = title
        self.imageURL = URL(string: imageURL)!
    }
    
}
