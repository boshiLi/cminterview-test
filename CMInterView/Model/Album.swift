//
//  Album.swift
//  CMInterView
//
//  Created by Boshi Li on 2019/11/5.
//  Copyright © 2019 Boshi Li. All rights reserved.
//

import Foundation

struct Album: Decodable {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
